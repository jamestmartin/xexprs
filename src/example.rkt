#lang racket
#|
    Copyright 2017-2019 James Martin

    This file is part of Xexprs.

    Xexprs is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Xexprs is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Xexprs.  If not, see <http://www.gnu.org/licenses/>.
|#
(require "htmlgen.rkt")

; The site's nav bar, a semantic unit of the page.
; This nav bar is just a normal nav bar, including the brand name in a stylized font.
(define site-nav
  ; I could change the definition of entry easily, and I have before (it was more complex).
  (let ([entry (λ (href name) `(a #:href ,href ,name))])
    `(nav (span #:id "navbrand" "Brand Name")
          ; Pretty convenient, and a lot easier to maintain than doing it manually!
          ,(render-list (entry "/" "Home")
                        (entry "/music/" "Music")
                        (entry "/games/" "Games")
                        (entry "/about/" "About")))))

; A function generating a page including the website's typical attributes.
; It's basically equivalent to the html function, only with a bunch of default
; meta tags, and the site header built in.
(define (site-page body
                   (head '())
                   #:author (author #f)
                   #:title (title #f))
  (html `(head
          (meta #:charset "utf-8")
          (meta #:name "viewport" #:content "width=device-width, initial-scale=1")
          (link #:href "/favicon.png" #:rel "icon" #:type "image/png")
          (link #:href "/res/common.css" #:rel "stylesheet" #:type "text/css")
          (meta #:name "author" #:content ,author)
          (title ,title)
          ,@head)
        `(body
          ,site-nav
          ,body)))

; Makes a list of numbers counting down from n to 1, e.g. 10 9 8 7 6 .. 1.
; There's probably a function to avoid explicit recursion, but I didn't see it
; in the reference, believe it or not. If you know, please submit a pull request.
(define (count-down-from n)
  (if (= 0 n) '()
      (cons n (count-down-from (- n 1)))))

; An example page, which I hope to grow more later.
(define my-page
  (site-page #:title "Hello, World!"
             #:author "Lijero"
             ; Pretty contrived, but an okay temporary example of "dynamic" content.
             ; You can easily see it being e.g. generated from the URL query or a DB.
             (apply render-list
                    (map number->string
                         (count-down-from 10)))))

(display my-page)
(display-to-file #:exists 'replace my-page "test.xhtml")